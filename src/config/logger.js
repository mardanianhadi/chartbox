const { join } = require("path");

module.exports = {
    dev: {
        level: "trace",
        prettyPrint: {
            colorize: false,
            ignore: 'pid,time,hostname,level',
            messageFormat: require("../tools/logger").formatter
        }
    },
    production: {
        level: "error",
        file: join(__dirname, "../../logs/error.log")
    }
}