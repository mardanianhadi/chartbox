function Pipe(app, options, done) {
    app.pipes.kraken.broadcast = function (raw) {
        if (raw.utf8Data.slice(0, 1) === "[") {
            const data = JSON.parse(raw.utf8Data);
            app.io.of("/tick").emit("tick", data[1].a[0])
        }
    }
    done();
}

module.exports = require("fastify-plugin")(Pipe);

