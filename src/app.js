const fastify = require("fastify").default,
    autoload = require("@fastify/autoload"),
    { join } = require("path");

module.exports = async (options = {}) => {
    require("dotenv").config({
        path: join(__dirname, "config/.env." + process.env.NODE_ENV)
    });

    const app = fastify(options);

    app.register(autoload, { dir: join(__dirname, "plugins") });
    app.register(autoload, { dir: join(__dirname, "pipes") });

    app.addHook("onResponse", require("./tools/logger").onResponse);

    return app;
}