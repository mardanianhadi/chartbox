function Plugin(app, options, next) {
    app.decorate("pipes", {
        tick: {},
        kraken: {}
    });

    next();
}

module.exports = require("fastify-plugin")(Plugin);