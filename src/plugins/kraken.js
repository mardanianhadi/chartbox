const Client = require("websocket").client,
    config = require("../config/kraken.json");

async function Plugin(app, options) {
    app.decorate("kraken_ws", {});
    
	const Kraken = new Client();
        
	Kraken.connect(process.env.KRAKEN_WS_ADDRESS, 'echo-protocol');

    Kraken.on("connect", connection => {
        app.log.info("Connected to kraken");

        connection.sendUTF(JSON.stringify(config));
        connection.on("message", app.pipes.kraken.broadcast);
    });
	
}

module.exports = require("fastify-plugin")(Plugin)