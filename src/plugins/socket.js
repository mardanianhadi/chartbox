const { createAdapter } = require("@socket.io/redis-adapter");

function Plugin(server, options, next) {
    server.io.adapter(createAdapter(server.redis, server.redis.duplicate()));
    server.io.of("/tick").use((socket, done) => {
        if(socket.handshake.headers?.token !== "abcd")
            return done(new Error("SOCKET_ACCESS_DENIED"));
        done()
    });
    next();
}

module.exports = require("fastify-plugin")(Plugin);