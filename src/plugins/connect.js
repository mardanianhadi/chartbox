async function Plugin(app, options) {
    await app.register(require("fastify-redis"), {
		host: process.env.REDIS_HOST,
		port: process.env.REDIS_PORT,
		password: process.env.REDIS_PASSWORD,
	});
    app.log.info("Connected to Redis");

    await app.register(require("fastify-socket.io"), {
        serveClient: false,
        transports: ["polling", "websocket"],
		cors: {
			origin: "*",
			methods: ["GET", "POST"]
		} 
    });
    app.log.info("Socket.io is up");
}

module.exports = require("fastify-plugin")(Plugin);