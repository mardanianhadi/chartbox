const colorette = require("colorette"),
    LEVELS = {
        10: "Response",
        20: "Debug",
        30: "Info",
        40: "Warn",
        50: "Error",
        60: "Fetal"
    };

const formatter = ({ level, time, msg }) => {
    const date = new Date(time),
        levelString = `[ ${LEVELS[level]} ]`,
        datetime = ` - ${date.toISOString().split('T')[0]} - ${date.toLocaleTimeString("en-US", { hour12: false })}:${date.getMilliseconds()}`;
        
    switch (level) {
        case 10:
            return "\n" + colorette.blue(levelString + datetime) + `\n${msg}`;
        case 50:
            return "\n" + colorette.red(levelString + datetime) + `\n${msg}`;
        default:
            return "\n" + colorette.green(levelString + datetime) + `\n${msg}`;
    };
};

const onResponse = function(request, reply, next){
    request.url !== '/healthy' && this.log.trace(`${request.ip} - ${request.method.toUpperCase()} - ${reply.statusCode} - ${request.url}`);
    next();
};

module.exports = { formatter, onResponse };