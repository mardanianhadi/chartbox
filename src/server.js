const loggerConfig = require('./config/logger');
const start = async () => {
    try {
        const server = await require("./app")({
            disableRequestLogging: true,
            logger: loggerConfig[process.env.NODE_ENV]
        }),
            PORT = process.env.PORT || 5004,
            HOST = process.env.HOST || "0.0.0.0";

        await server.listen({ port: PORT, host: HOST });
        
    } catch (error) {
        console.error(error);
        process.exit(1);
    }
};

start();